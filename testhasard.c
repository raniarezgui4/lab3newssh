// Fichier testhasard.c
#include <assert.h>
#include "jeu.h"

int main(){
  char c = hasard();
  assert(c == 'R' || c == 'P' || c == 'C');
  
  return 0;
}
