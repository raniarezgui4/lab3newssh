CC=gcc
CFLAGS=-std=c99 -Wall

#additional flags for gcov
TESTFLAGS=-fprofile-arcs -ftest-coverage

testhasard: testhasard.c jeu.h jeu.c
	# build the hasard test
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testhasard testhasard.c jeu.c

	# run the hasard test, which will generate testhasard.gcna and testhasard.gcno
	./testhasard

	# compute how test is covering testhasard.c
	gcov -c -p testhasard
clean:
	rm -f *.o testcomparaison testhasard *.gcov *gcda *.gcno
