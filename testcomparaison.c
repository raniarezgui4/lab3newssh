#include "jeu.h"
#include <assert.h>

int main(int argc, char **argv)
{
	assert(comparaison('R' , 'R') == 0);
	assert(comparaison('C' , 'C') == 0);
	assert(comparaison('P' , 'P') == 0);
	assert(comparaison('R' , 'C') == 1);
	assert(comparaison('C' , 'P') == 1);
	assert(comparaison('P' , 'R') == 1);
	assert(comparaison('R' , 'P') == -1);
	assert(comparaison('C' , 'R') == -1);
	assert(comparaison('P' , 'C') == -1);

	return 0;

}
